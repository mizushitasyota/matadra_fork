package mataDra.logics;

public abstract class DAOLogic<T> {

	private T dao;//データオブジェクト
	private int count;//データオブジェクトのカウント
	private String result;//出力する文字列

	//表示するオブジェクトを返す抽象クラス
	public abstract String convert(int index);



	//ゲッターセッター自動生成
	public T getDao() {
		return dao;
	}
	public void setDao(T dao) {
		this.dao = dao;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}


	//インデックスの数が正しいかチェックする
	public String checkIndex(int index) {

		if (index >= getCount() || index < 0) {
			return "NO DATA";
		} else {
			return convert(index);
		}
	}


	//インデックスが正しければオブジェクトを表示する
		public String exec(int index){
			return checkIndex(index);
		}






}
