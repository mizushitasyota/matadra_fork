package mataDra.logics.battles;

import mataDra.entity.creatures.Character;
import mataDra.entity.creatures.Condition;
import mataDra.entity.creatures.Condition00;
import mataDra.view.ui.MessageArea;

public class Guard {
    private Condition condition = new Condition00();

    /**ガードを使用
     * @param c
     */
    public void useGuard(Character c){
    	//防御コマンド実行時防御力2倍
        c.setVitality(c.getMaxVitality()*2);
        c.getConditions().add(this.condition);
        String msg = c.getName()+"は"+condition.getMsg();
        MessageArea.message(msg);

    }
    /**ガード状態をリセット
     * @param c
     */
    public void resetGuard(Character c){
        c.setVitality(c.getMaxVitality());
        int index = c.getConditions().indexOf(condition);
        c.getConditions().remove(index);
    }

}
