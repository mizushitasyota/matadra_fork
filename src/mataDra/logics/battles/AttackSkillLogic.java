package mataDra.logics.battles;

import java.util.List;

import assets.items.equip.Armor;
import mataDra.entity.ability.SkillEntity;
import mataDra.entity.ability.SkillEntity.SkillType;
import mataDra.entity.creatures.Character;
import mataDra.view.ui.MessageArea;
import mataDra.viewss.ui.ImagePlayer;
import mataDra.viewss.ui.SoundPlayer;

/**
 * スキル攻撃
 *
 *
 */
public class AttackSkillLogic extends AttackLogic {

    /**
     * 単体攻撃設定
     *
     * @param offenceCharacter
     * @param abilityIndex
     * @param defenceCharacter
     * @param partyIndex
     */
    public void setAttack(Character oc, int abilityIndex, List<Character> dcs) {
        Character dc;// ディフェンスキャラクター
        String msg = "";//メッセージ
        int bonus;//ステータスボーナス
        int point;//攻撃力
        SkillEntity skill;//スキルクラス
        Armor armor;//防具クラス
        int attack;//総合攻撃力
        int defence;//総合防御力
        int hit;//命中率
        boolean isMiss;//命中判定
        int damage;//最終ダメージ
        // ディフェンスパーティの数だけ繰り返す。
        for (int i = 0; i < dcs.size(); i++) {
            dc = dcs.get(i);
            skill = oc.getSkills().get(abilityIndex);
            armor = dc.getArmor();
            bonus = getBonus(oc.getTechnique());
            point = skill.getPoint();
            // ブーメラン系スキルの場合、攻撃力減衰、それ以外は減衰なしの三項演算子。
            double damping = skill.getSkillType().equals(SkillType.throwing) ? calcDampingRate(dcs.size()) : 1;
            attack = (int) (calcTotalAttack(oc.getStrength(), bonus, point) * damping);
            defence = calcTotalDefence(dc.getVitality(), armor.getPoint());
            hit = calcAttackHitRate(oc.getAgility(), dc.getAgility());
            isMiss = isAttackMiss(hit);
            damage = calcTotalDamage(attack, defence);

            // 攻撃開始
            msg = oc.getName() + "は" + skill.getName() + "をはなった。";
            msg = skill.getMsg();
            MessageArea.message(msg);
            ImagePlayer.play(skill.getImage());
            SoundPlayer.play(skill.getSound());
            AttackDamageMessage(dc, damage, isMiss);

        }
    }

}
